# Generate CFS App

## Dependencies
This application requires Python 3.

* Jinja2
* MarkupSafe

Install dependencies using requirements.txt:
```bash
pip install -r requirements.txt
```

## How to Use

Clone the repository:
```bash
$ git clone https://gitlab.com/tjreverb/generate-cfs-app.git
```

Create a new application:
```bash
python generate_app.py
```

Create a new library:
```bash
python generate_library.py
```

Find the new app or library within the output folder.