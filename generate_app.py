from jinja2 import Environment, FileSystemLoader
import glob
import os
import pathlib
from pprint import pprint

###########################
#   Inputs
###########################
data = {}

def obtain_inputs():
    app_name = input("App Name: ").replace(" ", "_")
    data["name_lc"] = app_name.lower()
    data["name_uc"] = app_name.upper()

###########################
#   Templates
###########################
def read_in_templates():
    templates = {filename[10:]:None for filename in glob.iglob('templates/sample_app/**/*', recursive=True)
        if os.path.isfile(filename)}
    pprint(templates)
    return templates
    
def fill_in_templates(templates):
    new_app_files = {}
    for filename in templates:
        template = env.get_template(filename)
        new_app_files[filename.replace("sample", data["name_lc"])] = template.render(**data)
    return new_app_files
        
def export_templates(new_app_files):
    for filename in new_app_files:
        pathlib.Path("output/{}".format(filename[:filename.rfind('/')])).mkdir(parents=True, exist_ok=True)
        with open("output/{}".format(filename),"w+") as f:
            f.write(new_app_files[filename])


if __name__ ==  "__main__" :
    file_loader = FileSystemLoader("templates")
    env = Environment(loader=file_loader)

    obtain_inputs()

    templates = read_in_templates()
    new_app_files = fill_in_templates(templates)
    export_templates(new_app_files)