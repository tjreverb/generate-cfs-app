/*************************************************************************
** File:
**   $Id: {{name_lc}}_lib.c $
**
** Purpose: 
**   Sample CFS library
**
*************************************************************************/

/*************************************************************************
** Includes
*************************************************************************/
#include "{{name_lc}}_lib.h"
#include "{{name_lc}}_lib_version.h"

/*************************************************************************
** Macro Definitions
*************************************************************************/


/*************************************************************************
** Private Function Prototypes
*************************************************************************/
int32 {{name_uc}}_LibInit(void);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                 */
/* Library Initialization Routine                                  */
/* cFE requires that a library have an initialization routine      */ 
/*                                                                 */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int32 {{name_uc}}_LibInit(void)
{
    
    OS_printf ("{{name_uc}} Lib Initialized.  Version %d.%d.%d.%d",
                {{name_uc}}_LIB_MAJOR_VERSION,
                {{name_uc}}_LIB_MINOR_VERSION, 
                {{name_uc}}_LIB_REVISION, 
                {{name_uc}}_LIB_MISSION_REV);
                
    return CFE_SUCCESS;
 
}/* End {{name_uc}}_LibInit */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                 */
/* Sample Lib function                                             */ 
/*                                                                 */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int32 {{name_uc}}_Function( void ) 
{
   OS_printf ("{{name_uc}}_Function called\n");

   return(CFE_SUCCESS);
   
} /* End {{name_uc}}_Function */

/************************/
/*  End of File Comment */
/************************/
