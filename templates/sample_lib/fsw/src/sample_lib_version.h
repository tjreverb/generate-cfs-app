/************************************************************************
** File:
**   $Id: {{name_lc}}_lib_version.h $
**
** Purpose: 
**  The {{name_uc}} Lib header file containing version number
**
** Notes:
**
*************************************************************************/
#ifndef _{{name_lc}}_lib_version_h_
#define _{{name_lc}}_lib_version_h_


#define {{name_uc}}_LIB_MAJOR_VERSION    1
#define {{name_uc}}_LIB_MINOR_VERSION    0
#define {{name_uc}}_LIB_REVISION         0
#define {{name_uc}}_LIB_MISSION_REV      0

      
#endif /* _{{name_lc}}_lib_version_h_ */

/************************/
/*  End of File Comment */
/************************/
