/************************************************************************
** File:
**   $Id: {{name_lc }}_app_msgids.h  $
**
** Purpose: 
**  Define Sample App  Message IDs
**
** Notes:
**
**
*************************************************************************/
#ifndef _{{name_lc }}_app_msgids_h_
#define _{{name_lc }}_app_msgids_h_

#define {{name_uc}}_APP_CMD_MID            	0x1882
#define {{name_uc}}_APP_SEND_HK_MID        	0x1883
#define {{name_uc}}_APP_HK_TLM_MID		0x0883

#endif /* _{{name_lc }}_app_msgids_h_ */

/************************/
/*  End of File Comment */
/************************/
