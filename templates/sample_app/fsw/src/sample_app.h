/*******************************************************************************
** File: {{ name_lc }}_app.h
**
** Purpose:
**   This file is main hdr file for the {{name__uc}} application.
**
**
*******************************************************************************/

#ifndef _{{ name_lc }}_app_h_
#define _{{ name_lc }}_app_h_

/*
** Required header files.
*/
#include "cfe.h"
#include "cfe_error.h"
#include "cfe_evs.h"
#include "cfe_sb.h"
#include "cfe_es.h"

#include <string.h>
#include <errno.h>
#include <unistd.h>

/***********************************************************************/

#define {{name__uc}}_PIPE_DEPTH                     32

/************************************************************************
** Type Definitions
*************************************************************************/

/****************************************************************************/
/*
** Local function prototypes.
**
** Note: Except for the entry point ({{name__uc}}_AppMain), these
**       functions are not called from any other source module.
*/
void {{name__uc}}_AppMain(void);
void {{name__uc}}_AppInit(void);
void {{name__uc}}_ProcessCommandPacket(void);
void {{name__uc}}_ProcessGroundCommand(void);
void {{name__uc}}_ReportHousekeeping(void);
void {{name__uc}}_ResetCounters(void);

boolean {{name__uc}}_VerifyCmdLength(CFE_SB_MsgPtr_t msg, uint16 ExpectedLength);

#endif /* _{{ name_lc }}_app_h_ */
