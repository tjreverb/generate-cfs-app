/************************************************************************
** File:
**   $Id: {{ name_lc }}_app_version.h  $
**
** Purpose: 
**  The Application header file containing version number
**
** Notes:
**
**
*************************************************************************/
#ifndef _{{ name_lc }}_app_version_h_
#define _{{ name_lc }}_app_version_h_

#define {{ name_uc }}_APP_MAJOR_VERSION    1
#define {{ name_uc }}_APP_MINOR_VERSION    0
#define {{ name_uc }}_APP_REVISION         0
#define {{ name_uc }}_APP_MISSION_REV      0
      
#endif /* _{{ name_lc }}_app_version_h_ */

/************************/
/*  End of File Comment */
/************************/
