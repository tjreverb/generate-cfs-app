/*******************************************************************************
** File:
**   {{ name_lc }}_app_msg.h 
**
** Purpose: 
**  Define {{ name_uc }} App  Messages and info
**
** Notes:
**
**
*******************************************************************************/
#ifndef _{{ name_lc }}_app_msg_h_
#define _{{ name_lc }}_app_msg_h_

/*
** {{ name_uc }} App command codes
*/
#define {{ name_uc }}_APP_NOOP_CC                 0
#define {{ name_uc }}_APP_RESET_COUNTERS_CC       1

/*************************************************************************/
/*
** Type definition (generic "no arguments" command)
*/
typedef struct
{
   uint8    CmdHeader[CFE_SB_CMD_HDR_SIZE];

} {{ name_uc }}_NoArgsCmd_t;

/*************************************************************************/
/*
** Type definition ({{ name_uc }} App housekeeping)
*/
typedef struct 
{
    uint8              TlmHeader[CFE_SB_TLM_HDR_SIZE];
    uint8              {{ name_lc }}_command_error_count;
    uint8              {{ name_lc }}_command_count;
    uint8              spare[2];

}   OS_PACK {{ name_lc }}_hk_tlm_t  ;

#define {{ name_uc }}_APP_HK_TLM_LNGTH   sizeof ( {{ name_lc }}_hk_tlm_t )

#endif /* _{{ name_lc }}_app_msg_h_ */

/************************/
/*  End of File Comment */
/************************/
