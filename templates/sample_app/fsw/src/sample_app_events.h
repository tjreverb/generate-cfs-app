/************************************************************************
** File:
**    {{name_lc}}_app_events.h 
**
** Purpose: 
**  Define {{name_uc}} App Events IDs
**
** Notes:
**
**
*************************************************************************/
#ifndef _{{name_lc}}_app_events_h_
#define _{{name_lc}}_app_events_h_


#define {{name_uc}}_RESERVED_EID              0
#define {{name_uc}}_STARTUP_INF_EID           1 
#define {{name_uc}}_COMMAND_ERR_EID           2
#define {{name_uc}}_COMMANDNOP_INF_EID        3 
#define {{name_uc}}_COMMANDRST_INF_EID        4
#define {{name_uc}}_INVALID_MSGID_ERR_EID     5 
#define {{name_uc}}_LEN_ERR_EID               6 

#endif /* _{{name_lc}}_app_events_h_ */

/************************/
/*  End of File Comment */
/************************/
