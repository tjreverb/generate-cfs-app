/*******************************************************************************
** File: {{ name_lc }}_app.c
**
** Purpose:
**   This file contains the source code for the App.
**
*******************************************************************************/

/*
**   Include Files:
*/

#include "{{ name_lc }}_app.h"
#include "{{ name_lc }}_app_perfids.h"
#include "{{ name_lc }}_app_msgids.h"
#include "{{ name_lc }}_app_msg.h"
#include "{{ name_lc }}_app_events.h"
#include "{{ name_lc }}_app_version.h"

/*
** global data
*/

{{ name_lc }}_hk_tlm_t    {{ name_uc }}_HkTelemetryPkt;
CFE_SB_PipeId_t    {{ name_uc }}_CommandPipe;
CFE_SB_MsgPtr_t    {{ name_uc }}MsgPtr;

static CFE_EVS_BinFilter_t  {{ name_uc }}_EventFilters[] =
       {  /* Event ID    mask */
          { {{ name_uc }}_STARTUP_INF_EID,       0x0000},
          { {{ name_uc }}_COMMAND_ERR_EID,       0x0000},
          { {{ name_uc }}_COMMANDNOP_INF_EID,    0x0000},
          { {{ name_uc }}_COMMANDRST_INF_EID,    0x0000},
       };

/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* {{ name_uc }}_AppMain() -- Application entry point and main process loop          */
/*                                                                            */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * *  * * * * **/
void {{ name_uc }}_AppMain( void )
{
    int32  status;
    uint32 RunStatus = CFE_ES_APP_RUN;

    CFE_ES_PerfLogEntry({{ name_uc }}_APP_PERF_ID);

    {{ name_uc }}_AppInit();

    /*
    ** {{ name_uc }} Runloop
    */
    while (CFE_ES_RunLoop(&RunStatus) == TRUE)
    {
        CFE_ES_PerfLogExit({{ name_uc }}_APP_PERF_ID);

        /* Pend on receipt of command packet -- timeout set to 500 millisecs */
        status = CFE_SB_RcvMsg(&{{ name_uc }}MsgPtr, {{ name_uc }}_CommandPipe, 500);
        
        CFE_ES_PerfLogEntry({{ name_uc }}_APP_PERF_ID);

        if (status == CFE_SUCCESS)
        {
            {{ name_uc }}_ProcessCommandPacket();
        }

    }

    CFE_ES_ExitApp(RunStatus);

} /* End of {{ name_uc }}_AppMain() */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                            */
/* {{ name_uc }}_AppInit() --  initialization                                       */
/*                                                                            */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/
void {{ name_uc }}_AppInit(void)
{
    /*
    ** Register the app with Executive services
    */
    CFE_ES_RegisterApp() ;

    /*
    ** Register the events
    */ 
    CFE_EVS_Register({{ name_uc }}_EventFilters,
                     sizeof({{ name_uc }}_EventFilters)/sizeof(CFE_EVS_BinFilter_t),
                     CFE_EVS_BINARY_FILTER);

    /*
    ** Create the Software Bus command pipe and subscribe to housekeeping
    **  messages
    */
    CFE_SB_CreatePipe(&{{ name_uc }}_CommandPipe, {{ name_uc }}_PIPE_DEPTH,"{{ name_uc }}_CMD_PIPE");
    CFE_SB_Subscribe({{ name_uc }}_APP_CMD_MID, {{ name_uc }}_CommandPipe);
    CFE_SB_Subscribe({{ name_uc }}_APP_SEND_HK_MID, {{ name_uc }}_CommandPipe);

    {{ name_uc }}_ResetCounters();

    CFE_SB_InitMsg(&{{ name_uc }}_HkTelemetryPkt,
                   {{ name_uc }}_APP_HK_TLM_MID,
                   {{ name_uc }}_APP_HK_TLM_LNGTH, TRUE);

    CFE_EVS_SendEvent ({{ name_uc }}_STARTUP_INF_EID, CFE_EVS_INFORMATION,
               "{{ name_uc }} App Initialized. Version %d.%d.%d.%d",
                {{ name_uc }}_APP_MAJOR_VERSION,
                {{ name_uc }}_APP_MINOR_VERSION, 
                {{ name_uc }}_APP_REVISION, 
                {{ name_uc }}_APP_MISSION_REV);
				
} /* End of {{ name_uc }}_AppInit() */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/
/*  Name:  {{ name_uc }}_ProcessCommandPacket                                        */
/*                                                                            */
/*  Purpose:                                                                  */
/*     This routine will process any packet that is received on the {{ name_uc }}    */
/*     command pipe.                                                          */
/*                                                                            */
/* * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * *  * *  * * * * */
void {{ name_uc }}_ProcessCommandPacket(void)
{
    CFE_SB_MsgId_t  MsgId;

    MsgId = CFE_SB_GetMsgId({{ name_uc }}MsgPtr);

    switch (MsgId)
    {
        case {{ name_uc }}_APP_CMD_MID:
            {{ name_uc }}_ProcessGroundCommand();
            break;

        case {{ name_uc }}_APP_SEND_HK_MID:
            {{ name_uc }}_ReportHousekeeping();
            break;

        default:
            {{ name_uc }}_HkTelemetryPkt.{{ name_lc }}_command_error_count++;
            CFE_EVS_SendEvent({{ name_uc }}_COMMAND_ERR_EID,CFE_EVS_ERROR,
			"{{ name_uc }}: invalid command packet,MID = 0x%x", MsgId);
            break;
    }

    return;

} /* End {{ name_uc }}_ProcessCommandPacket */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/
/*                                                                            */
/* {{ name_uc }}_ProcessGroundCommand() -- {{ name_uc }} ground commands                    */
/*                                                                            */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/

void {{ name_uc }}_ProcessGroundCommand(void)
{
    uint16 CommandCode;

    CommandCode = CFE_SB_GetCmdCode({{ name_uc }}MsgPtr);

    /* Process "known" {{ name_uc }} app ground commands */
    switch (CommandCode)
    {
        case {{ name_uc }}_APP_NOOP_CC:
            {{ name_uc }}_HkTelemetryPkt.{{ name_lc }}_command_count++;
            CFE_EVS_SendEvent({{ name_uc }}_COMMANDNOP_INF_EID,CFE_EVS_INFORMATION,
			"{{ name_uc }}: NOOP command");
            break;

        case {{ name_uc }}_APP_RESET_COUNTERS_CC:
            {{ name_uc }}_ResetCounters();
            break;

        /* default case already found during FC vs length test */
        default:
            break;
    }
    return;

} /* End of {{ name_uc }}_ProcessGroundCommand() */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/
/*  Name:  {{ name_uc }}_ReportHousekeeping                                              */
/*                                                                            */
/*  Purpose:                                                                  */
/*         This function is triggered in response to a task telemetry request */
/*         from the housekeeping task. This function will gather the Apps     */
/*         telemetry, packetize it and send it to the housekeeping task via   */
/*         the software bus                                                   */
/* * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * *  * *  * * * * */
void {{ name_uc }}_ReportHousekeeping(void)
{
    CFE_SB_TimeStampMsg((CFE_SB_Msg_t *) &{{ name_uc }}_HkTelemetryPkt);
    CFE_SB_SendMsg((CFE_SB_Msg_t *) &{{ name_uc }}_HkTelemetryPkt);
    return;

} /* End of {{ name_uc }}_ReportHousekeeping() */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/
/*  Name:  {{ name_uc }}_ResetCounters                                               */
/*                                                                            */
/*  Purpose:                                                                  */
/*         This function resets all the global counter variables that are     */
/*         part of the task telemetry.                                        */
/*                                                                            */
/* * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * *  * *  * * * * */
void {{ name_uc }}_ResetCounters(void)
{
    /* Status of commands processed by the {{ name_uc }} App */
    {{ name_uc }}_HkTelemetryPkt.{{ name_lc }}_command_count       = 0;
    {{ name_uc }}_HkTelemetryPkt.{{ name_lc }}_command_error_count = 0;

    CFE_EVS_SendEvent({{ name_uc }}_COMMANDRST_INF_EID, CFE_EVS_INFORMATION,
		"{{ name_uc }}: RESET command");
    return;

} /* End of {{ name_uc }}_ResetCounters() */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/
/*                                                                            */
/* {{ name_uc }}_VerifyCmdLength() -- Verify command packet length                   */
/*                                                                            */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/
boolean {{ name_uc }}_VerifyCmdLength(CFE_SB_MsgPtr_t msg, uint16 ExpectedLength)
{     
    boolean result = TRUE;

    uint16 ActualLength = CFE_SB_GetTotalMsgLength(msg);

    /*
    ** Verify the command packet length.
    */
    if (ExpectedLength != ActualLength)
    {
        CFE_SB_MsgId_t MessageID   = CFE_SB_GetMsgId(msg);
        uint16         CommandCode = CFE_SB_GetCmdCode(msg);

        CFE_EVS_SendEvent({{ name_uc }}_LEN_ERR_EID, CFE_EVS_ERROR,
           "Invalid msg length: ID = 0x%X,  CC = %d, Len = %d, Expected = %d",
              MessageID, CommandCode, ActualLength, ExpectedLength);
        result = FALSE;
        {{ name_uc }}_HkTelemetryPkt.{{ name_lc }}_command_error_count++;
    }

    return(result);

} /* End of {{ name_uc }}_VerifyCmdLength() */

