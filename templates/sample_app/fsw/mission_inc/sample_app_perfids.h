/************************************************************************
** File:
**   $Id: {{ name_lc }}_app_perfids.h  $
**
** Purpose: 
**  Define App Performance IDs
**
** Notes:
**
*************************************************************************/
#ifndef _{{ name_lc }}_app_perfids_h_
#define _{{ name_lc }}_app_perfids_h_


#define {{ name_uc }}_APP_PERF_ID              91 

#endif /* _{{ name_lc }}_app_perfids_h_ */

/************************/
/*  End of File Comment */
/************************/
